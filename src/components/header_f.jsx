import React from 'react';
import logoimage from '../svg/logoimage.svg';
import styles from '../css components/header_f.module.css';

const First_header = () => {
    return(
        <header className={styles.header_first}>
            <div className={styles.header_wrapper}>
                <div className={styles.header_logo}>
                    <a href="#" className={styles.header_logo_link}>
                        <img src={logoimage} alt="logo" className={styles.logoimage} />
                    </a>
                </div>
                <div className={styles.header_menu}>
                    <nav className={styles.header_nav}>
                        <ul className={styles.header_list}>
                            <li className={styles.header_item}>
                                <a href="#" className={styles.header_link}>Товары</a>
                            </li>
                            <li className={styles.header_item}>
                                <a href="#" className={styles.header_link}>Услуги</a>
                            </li>
                            <li className={styles.header_item}>
                                <a href="#" className={styles.header_link}>Контакты</a>
                            </li>
                            <li className={styles.header_item}>
                                <a href="#" className={styles.header_link}>Обратная сявзь</a>
                            </li>
                            <li className={styles.header_item}>
                                <a href="#" className={styles.header_link}>Доставка</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                    <button className={styles.header_button}>Личный кабинет</button>
            </div>
            <div className={styles.header_text}>
                <p className={styles.header_content}>Чехлы</p>
                <p className={styles.header_content}>Аксессуары</p>
                <p className={styles.header_content}>Периферийные устройства</p>
                <p className={styles.header_descr}>Закажите аксессуары для вашего телефона у нас и получите скидку на первый заказ 10%</p>
            </div>
            <button className={styles.text_button}>Начать покупки</button>
        </header>
    )
}

export default First_header;