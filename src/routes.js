import React from 'react';
import {Route, Switch} from 'react-router-dom'
import Footer from './components/footer';
import First_header from './components/header_f';
import Delivery from './components/delivery';
import Contact from './components/contact'
import Header from './components/header'
import Question from './components/footer_question'
import Persona from './components/personal_area'
import Liked from './components/liked-products'
import Product from './components/product'

export const Routes = () =>{
    return(
        <Switch>
            <Route path={'/'} exact>
                <First_header />
                <Footer />
            </Route>
        </Switch>
    )
}

export default Routes;