import './App.css';
import Footer from './components/footer';
import First_header from './components/header_f';
import Delivery from './components/delivery';
import Contact from './components/contact'
import Header from './components/header'
import Question from './components/footer_question'
import Persona from './components/personal_area'
import Liked from './components/liked-products'
import Product from './components/product'
import Routes from './routes.js'

function App() {
  return (
    <div >
      <Routes/>
    </div>
  );
}

export default App;
