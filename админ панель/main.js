window.onload = function () {

    function callPopup () {
        const btnsOpen = document.querySelectorAll(".catalogue__btn-nav");
        const btnsClose = document.querySelectorAll(".close");

        btnsOpen.forEach(btn => {
            btn.addEventListener("click", () => {
                let id = btn.dataset.callPopup;

                document.querySelectorAll('.popup').forEach(popup => {
                    popup.classList.add("hide");
                });

                document.querySelectorAll('.table').forEach(table => {
                    table.classList.add("hide");
                });

                document.querySelector(`${id}`).classList.remove("hide");
                
            });
        });

        btnsClose.forEach(btn => {
            btn.addEventListener("click", () => {
                document.querySelector(`#popup1`).classList.add("hide");
                document.querySelector(`#popup2`).classList.add("hide");
            });
        });
    }

    function switchTabs () {
        const tabsContent = document.querySelectorAll(".table");

        document.querySelectorAll(".catalogue__tab").forEach(tab => {

            tab.addEventListener("click", () => {
                tabsContent.forEach(tabContent => {
                    tabContent.classList.add("hide");
                });

                let id = tab.dataset.tab;

                

                document.querySelector(`${id}`).classList.remove("hide");

            });
        });
    }

    callPopup ();
    switchTabs ();
}